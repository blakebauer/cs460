﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hw4.Controllers
{
    public class HomeController : Controller
    {

        /// <summary>
        /// Home page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        //Page 1 start
        /// <summary>
        /// Page 1: Checks to see if there is any input
        /// if so will convert input string to a different base
        /// </summary>
        /// <returns></returns>
        public ActionResult BaseConvert() 
        {
            //Get input
            string input = Request.QueryString["input"];
            int inBase = Convert.ToInt32(Request.QueryString["inBase"]);
            int outBase = Convert.ToInt32(Request.QueryString["outBase"]);

            //Check to see if we have input
            if (input == null || input == "")
            {
                return View();
            }

            //Verify Bases are within bounds
            if (inBase <= 1 || inBase > 62 || outBase <= 1 || outBase > 62)
            {
                ViewBag.error = "Input or Output Base are not between 0 and 62";
                return View();
            }

            long output = 0;
            int i = 0;
            foreach (char c in input.Reverse())
            {
                //Get int value of c
                int value = GetValueOf(c);

                //Check to make sure input's base matches inBase
                if (value == -1 || value >= inBase) 
                {
                    ViewBag.error = "Input does not match input base";
                    return View();
                }
                else
                {
                    //Convert value to base 10
                    output += (long)(value * Math.Pow(inBase, i));
                    i++;
                }
            }

            //Calculate the exponent to start at
            long exp = (long)Math.Floor(Math.Log(output, outBase));
            long num;
            int divide;
            string outputS = "";

            
            for (; exp >= 0; exp--)
            {
                //Calculate outBase ^ exp 
                num = (long)Math.Pow(outBase, exp); 

                //How many nums go into output determines digits
                divide = (int)Math.Floor((double)output / num);

                //Remove all num's from output
                output = (long)(output % num);

                //Append to string
                outputS += GetChar(divide);
            }

            ViewBag.input = input;
            ViewBag.inBase = inBase;
            ViewBag.outBase = outBase;
            ViewBag.output = outputS;

            return View();
        }

        /// <summary>
        /// Converts a character to a int value for 
        /// calculation based on a scheme. Should work
        /// with any character system assuming each set
        /// is consecutive.
        /// </summary>
        /// <param name="i">The character</param>
        /// <returns>Value of i</returns>
        private int GetValueOf(char i)
        {
            if (i >= '0' && i <= '9')
                return i - '0';
            else if (i >= 'A' && i <= 'Z')
                return i - 'A' + 10;
            else if (i >= 'a' && i <= 'z')
                return i - 'A' + 36;
            return -1;
        }

        /// <summary>
        /// Converts an int value to its char representation
        /// based on a scheme.
        /// </summary>
        /// <param name="i">number to convert</param>
        /// <returns>Char representation of i</returns>
        private char GetChar(int i)
        {
            if (i >= 0 && i <= 9)
                return (char)(i + '0');
            else if (i >= 10 && i <= 35)
                return (char)(i + 'A' - 10);
            else if (i >= 36 && i <= 61)
                return (char)(i + 'a' - 36);
            return '!';
        }
        //Page 1 end

        //Page 2 start
        /// <summary>
        /// Page 2: Get
        /// </summary>
        /// <returns></returns>
        public ActionResult BulletDrop()
        {
            return View();
        }

        /// <summary>
        /// Page 2 post: calculates the bullet drop from 
        /// given input and send it to the view.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BulletDrop(FormCollection form)
        {
            //Convert input to doubles and check if they are valid
            if (!Double.TryParse(form["velocity"], out double vel) 
                || !Double.TryParse(form["distance"], out double distance)
                || !Double.TryParse(form["sight"], out double sight))
            {
                //Not sure if I like this or using Convert better
                ViewBag.error = "Inputs could not be parsed";
            }
            else
            {
                //Convert from yards to feet
                distance = distance * 3;
                sight = sight * 3;

                //Start angle of projectile
                double angle = Math.Asin(((32.2)*sight)/(vel*vel)) / 2;
                double velx = vel * Math.Cos(angle); 
                double vely = vel * Math.Sin(angle); 
                double time = distance / velx;
                double drop = (vely * time - 16.1 * time * time) * 12;

                ViewBag.time = Math.Round(time, 3);
                ViewBag.drop = Math.Round(Math.Abs(drop), 3);
            }

            return View();
        }
        //Page 2 end

        //Page 3 start
        public ActionResult Page3()
        {
            return View();
        }

        /// <summary>
        /// Calculates the payment per period and the total payment 
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="rate"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Page3(decimal? amount, decimal? rate, int? length)
        {
            if(!(amount == null || rate == null || length == null))
            {
                rate = rate / 100;
                decimal interest = (decimal) (1 - Math.Pow((double) (1 + rate),  - (int) length));
                decimal monthly = ((decimal)amount * (decimal)rate) / interest;
                ViewBag.monthly = monthly;
                ViewBag.total = monthly * length;
            }
            return View();
        }
        //Page 3 end
    }
}