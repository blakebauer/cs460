﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw3
{
    /// <summary>
    /// A Stack Interface
    /// </summary>
    interface IStackADT
    {
        /// <summary>
        /// Push an item onto the top of the stack. Pushing an object
        /// that doesn't exist should result in an error. 
        /// </summary>
        /// <param name="newItem">The non-null object to push onto the stack</param>
        /// <returns>Reference to newItem, or null if newItem == null</returns>
        object Push(object newItem);
        
        /// <summary>
        /// Removes and returns the top item on the stack.
        /// Should error if stack is empty.
        /// </summary>
        /// <returns>Reference to object that was popped or null if stack is empty</returns>
        object Pop();

        /// <summary>
        /// Return the top item on the stack without removing it.
        /// Should error if stack is empty or return null.
        /// </summary>
        /// <returns>Reference to the top item or null if stack is empty</returns>
        object Peek();

        /// <summary>
        /// Query the stack to see if it is empty or not. Cannot produce an error.
        /// </summary>
        /// <returns>True if the stack is empty, false otherwise</returns>
        bool IsEmpty();

        /// <summary>
        /// Reset the stack by emptying it. The exact technique used to clear 
	    /// the stack is up to the implementor. The user should pay attention to what 
	    /// this behavior is.
        /// </summary>
        void Clear();
    }
}
