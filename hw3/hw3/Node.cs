﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw3
{
    /// <summary>
    /// A simple singly linked node class. 
    /// This is a none generic implementation.
    /// </summary>
    class Node
    {
        public object Data { get; set; }
        public Node Next { get; set; }

        public Node()
        {
            Data = null;
            Next = null;
        }

        public Node(object data, Node next)
        {
            Data = data;
            Next = next;
        }
    }
}
