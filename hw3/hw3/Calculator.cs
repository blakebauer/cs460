﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw3
{
    /// <summary>
    /// Command line postfix calculator.  This code makes use of ArgumentException
    /// to indicate when there is a problem with the input expression. 
    /// </summary>
    /// <remarks>
    /// Originally written by Scot Morse in Java
    /// ReWritten in C# by Blake Bauer
    /// </remarks>
    class Calculator
    {
        /// <summary>
        /// A datastructure to store operands for the postfix calculation.
        /// </summary>
        IStackADT stack = new LinkedStack();

        /// <summary>
        /// Entry point. No command line arguments.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Calculator app = new Calculator();
            bool playAgain = true;
            Console.WriteLine("\nPostfix Calculator. Recognizes these operators: + - * /");
            while (playAgain)
            {
                playAgain = app.DoCalulation();
            }
            Console.WriteLine("Bye.");
        }

        /// <summary>
        /// Get input string from user and perform calculation, returning true when
	    /// finished.If the user wishes to quit this method returns false.
        /// </summary>
        /// <returns>true if a calculation succeeded, false if the user wishes to quit</returns>
        private bool DoCalulation()
        {
            Console.WriteLine("Please enter q to quit\n");

            string input = "2 2 +";
            Console.Write("> ");

            input = Console.ReadLine();

            if (input.StartsWith("q") || input.StartsWith("Q"))
            {
                return false;
            }

            string output = "4";

            try
            {
                output = EvaluatePostFixInput(input);
            }
            catch (ArgumentException e)
            {
                output = e.Message;
            }
            Console.WriteLine("\n\t>>> " + input + " = " + output);
            return true;
        }

        /// <summary>
        /// Evaluate an arithmetic expression written in postfix form.
        /// </summary>
        /// <param name="input">Postfix mathematical expression as a string</param>
        /// <returns>Answer as a string</returns>
        /// <exception cref="ArgumentException">A Input error occured</exception>
        public string EvaluatePostFixInput(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException("Null or the empty string are not valid postfix expressions.");

            stack.Clear();

            String s;
            double a, b, c;
            string[] stuff = input.Split(' ');

            for (int i = 0; i < stuff.Length; ++i)
            {
                try
                {
                    double t = Convert.ToDouble(stuff[i]);
                    stack.Push(t);
                }
                catch (FormatException e)
                {
                    s = stuff[i];
                    if (s.Length > 1)
                        throw new ArgumentException("Input Error: " + s + " is not an allowed number or operator");

                    if (stack.IsEmpty())
                        throw new ArgumentException("Improper input format. Stack became empty when expecting second operand.");

                    b = Convert.ToDouble(stack.Pop());

                    if (stack.IsEmpty())
                        throw new ArgumentException("Improper input format. Stack became empty when expecting first operand.");

                    a = Convert.ToDouble(stack.Pop());

                    c = DoOperation(a, b, s);

                    stack.Push(c);
                }
            }

            return Convert.ToString(stack.Pop());
        }

        /// <summary>
        /// Perform arithmetic.  Put it here so as not to clutter up the previous method, which is already pretty ugly.
        /// </summary>
        /// <param name="a">First Operand</param>
        /// <param name="b">Second Operand</param>
        /// <param name="s">Operator</param>
        /// <returns>The answer</returns>
        /// <exception cref="ArgumentException">Wrong operator or b == 0</exception>
        public double DoOperation(double a, double b, string s)
        {
            double c = 0.0;
            switch (s)
            {
                case "+":
                    c = a + b;
                    break;
                case "-":
                    c = a - b;
                    break;
                case "*":
                    c = a * b;
                    break;
                case "/":
                    if (b == 0)
                        throw new ArgumentException("Can't divide by zero");
                    c = a / b;
                    break;
                default:
                    throw new ArgumentException("Improper operator: " + s + ", is not one of +, -, *, or /");
            }

            return c;
        }
    }
}