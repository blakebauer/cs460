﻿
function search() {

    // Send a ajax request
    var a = $.ajax({
        type: "GET",
        dataType: "json",
        url: "search/giphy",
        data: {
            // Parameters to the ajax method
            tags: $("#searchtext")[0].value,
            random: $("#random")[0].checked,
            still: $("#still")[0].checked,
            rating: $("#rating")[0].value,
            offset: $("#offset")[0].value
        }
    }).done(function () {
        // For each image
        a.responseJSON.forEach(function (i, index) {
            // Grab the indexed image or create it if it doesn't exist
            var images = $("#images div img");
            if (images.length > 5) {
                images[index].src = i;
            } else {
                $("#images").append(`
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <img class="gifimg" src=` + i + `>
                    </ div>
                `);
            }
            })
        }).fail(function (a,b,c) {
            console.log(c);
        });
}

// Disable offset selector when random is checked
function ranCheck(cb) {
    $("#offset")[0].disabled = cb.checked;
}


