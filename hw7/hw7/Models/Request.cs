namespace hw7.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Request
    {
        public int ID { get; set; }

        [Required]
        [StringLength(255)]
        public string Tags { get; set; }

        public bool IsRandom { get; set; }

        [Required]
        [StringLength(8)]
        public string Rating { get; set; }

        public DateTime RequestDate { get; set; }

        public bool IsMobile { get; set; }

        [Required]
        [StringLength(255)]
        public string IPAddress { get; set; }

        [Required]
        [StringLength(255)]
        public string UserAgent { get; set; }
    }
}
