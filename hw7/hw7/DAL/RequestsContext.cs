﻿using hw7.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace hw7.DAL
{
    public class RequestsContext : DbContext
    {
        public RequestsContext() : base("name=Request")
        { }

        public virtual DbSet<Request> Requests { get; set; }
    }
}