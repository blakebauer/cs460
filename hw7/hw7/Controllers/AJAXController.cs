﻿using hw7.DAL;
using hw7.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace hw7.Controllers
{
    public class AJAXController : Controller
    {
        [HttpGet]
        public JsonResult Search(string tags, bool random, bool still, int offset, string rating)
        {
            // Log the request to the database
            using(RequestsContext db = new RequestsContext())
            {
                Request req = new Request {
                    RequestDate = DateTime.Today,
                    Tags        = tags,
                    IsRandom    = random,
                    Rating      = rating,
                    IsMobile    = Request.Browser.IsMobileDevice,
                    IPAddress   = Request.UserHostAddress,
                    UserAgent   = Request.UserAgent
                };

                db.Requests.Add(req);
                db.SaveChanges();
            }

            // Get the super secret api key
            string apiKey = System.Web.Configuration.WebConfigurationManager.AppSettings["GiphyApiKey"];

            using (WebClient w = new WebClient())
            {
                string jsondata = "";
                string[] images = new string[6];

                try
                {
                    // Check if random is checked
                    if (random)
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            // Get a random image and add it to the list
                            jsondata = w.DownloadString($"https://api.giphy.com/v1/gifs/random?api_key={apiKey}&tag={tags}&rating={rating}");
                            dynamic json = new JavaScriptSerializer().DeserializeObject(jsondata);
                            if (still)
                                images[i] = (json["data"]["image_original_url"] as string).Replace("giphy.gif", "200_s.gif");
                            else
                                images[i] = (json["data"]["image_original_url"] as string).Replace("giphy.gif", "200.gif");
                        }
                    }
                    else
                    {
                        // Scale offset
                        offset = offset * 6;
                        // Get 6 images
                        jsondata = w.DownloadString($"https://api.giphy.com/v1/gifs/search?api_key={apiKey}&q={tags}&offset={offset}&limit=6&rating={rating}&lang=en");
                        dynamic json = new JavaScriptSerializer().DeserializeObject(jsondata);

                        // Add each image to the list
                        for (int i = 0; i < 6; i++)
                        {
                            if (still)
                                images[i] = json["data"][i]["images"]["fixed_height_still"]["url"] as string;
                            else
                                images[i] = json["data"][i]["images"]["fixed_height"]["url"] as string;
                        }
                    }
                }
                catch (Exception e)
                {

                }

                // Return the images
                return Json(images, JsonRequestBehavior.AllowGet);
            }
        }
    }
}