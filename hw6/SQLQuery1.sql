﻿RESTORE DATABASE AdventureWorks
FROM DISK = 'C:\Users\blake\Downloads\AdventureWorks2016.bak'
WITH MOVE 'AdventureWorks2016_Data' TO 'C:\Users\blake\Programming\CS460\homework\hw6\hw6\App_Data\AdventureWorks.mdf',
MOVE 'AdventureWorks2016_Log' TO 'C:\Users\blake\Programming\CS460\homework\hw6\hw6\App_Data\AdventureWorks.ldf'

RESTORE FILELISTONLY  
FROM DISK = 'C:\Users\blake\Downloads\AdventureWorks2016.bak'  
GO  