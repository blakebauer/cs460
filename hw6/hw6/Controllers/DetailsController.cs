﻿using hw6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hw6.Controllers
{
    public class DetailsController : Controller
    {

        /// <summary>
        /// Displays a page about the specified product
        /// Redirects to Browse if an id is not specified
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        public ActionResult Details(int? productID)
        {
            if (productID == null)
                return RedirectToAction("CategorySelect", "Browse");

            using (ProductionContext db = new ProductionContext())
            {
                var product = db.Products.Where(p => p.ProductID == productID).FirstOrDefault();
                product.ProductReviews = product.ProductReviews.ToList();
                return View(product);
            }
        }

        /// <summary>
        /// View for adding a review
        /// Redirects to Browse if an id is not specified
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        public ActionResult AddReview(int? productID)
        {
            if (productID == null)
                return RedirectToAction("CategorySelect", "Browse");

            using (ProductionContext db = new ProductionContext())
                ViewBag.ProductName = db.Products.Where(p => p.ProductID == productID).FirstOrDefault().Name;

            ViewBag.ProductID = productID;
            return View();
        }

        /// <summary>
        /// Post method for adding a review
        /// Add a review to the database and redirects to the products page
        /// if successful
        /// </summary>
        /// <param name="review"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddReview(ProductReview review)
        {
            if (ModelState.IsValid)
            {
                using (ProductionContext db = new ProductionContext())
                {
                    review.ReviewDate = DateTime.Today;
                    review.ModifiedDate = DateTime.Today;
                    db.ProductReviews.Add(review);
                    db.SaveChanges();
                }
                return RedirectToAction("Details", new { productID = review.ProductID });
            }

            return View(review.ProductID);
        }

        /// <summary>
        /// Returns a file object of an image for the specified productid
        /// </summary>
        /// <param name="productID"></param>
        /// <returns></returns>
        public ActionResult GetImage(int productID)
        {
            using (ProductionContext db = new ProductionContext())
            {
                byte[] img = db.ProductProductPhotoes.Where(d => d.ProductID == productID).Select(m => m.ProductPhoto).FirstOrDefault().LargePhoto;

                return File(img, "image/png");
            }
        }
    }
}