﻿using hw6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hw6.Controllers
{
    public class BrowseController : Controller
    {
        /// <summary>
        /// Browse products view needs a subCategoryID, it will be redirected to CategorySelect 
        /// if it does not have one
        /// </summary>
        /// <param name="subCategoryID"></param>
        /// <returns></returns>
        public ActionResult Browse(int? subCategoryID)
        {
            if (subCategoryID == null)
                return RedirectToAction("CategorySelect");

            using (ProductionContext db = new ProductionContext())
            {
                var subcategory = db.ProductSubcategories.Where(sc => sc.ProductSubcategoryID == subCategoryID).FirstOrDefault();

                ViewBag.Subcategory = subcategory.Name;
                ViewBag.Category = subcategory.ProductCategory.Name;

                return View(subcategory.Products.ToList());
            }
        }

        /// <summary>
        /// View for selecting the category
        /// </summary>
        /// <returns></returns>
        public ActionResult CategorySelect()
        {
            using (ProductionContext db = new ProductionContext())
            {
                LinkedList<CategoryVM> categories = new LinkedList<CategoryVM>();

                foreach (var cat in db.ProductCategories.ToList())
                {
                    categories.AddLast(new CategoryVM(cat));
                }

                return View(categories);
            }
        }
    }
}