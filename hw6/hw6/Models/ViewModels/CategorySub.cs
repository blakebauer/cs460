namespace hw6.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// Category view model that gets the list of subcategories for use in the view.
    /// </summary>
    public class CategoryVM
    {
        public CategoryVM(ProductCategory category)
        {
            Category = category;
            Subcategories = category.ProductSubcategories.ToList();
        }

        public IEnumerable<ProductSubcategory> Subcategories;
        public ProductCategory Category;
    }
}