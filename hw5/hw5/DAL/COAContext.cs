﻿using hw5.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace hw5.DAL
{
    /// <summary>
    /// Context class for the Change of Address database
    /// </summary>
    public class COAContext : DbContext
    {
        public COAContext() : base("name=OurDBContext")
        { }

        public virtual DbSet<COA> COAs { get; set; }
    }
}