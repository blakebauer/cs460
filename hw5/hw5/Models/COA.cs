﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hw5.Models
{
    /// <summary>
    /// Model for the Change of Address database
    /// </summary>
    public class COA
    {
        public int ID { get; set; }

        [Required, StringLength(32)]
        [Display(Name = "ODL/PERMIT/ID")]
        public string PermitID { get; set; }

        [Required, StringLength(128)]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        public DateTime DOB { get; set; }

        [Required, StringLength(256)]
        [Display(Name = "New Address")]
        public string NewAddress { get; set; }

        [Required, StringLength(64)]
        public string City { get; set; }

        [Required, StringLength(64)]
        public string State { get; set; }

        [Required, StringLength(10)]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [Required, StringLength(64)]
        public string County { get; set; }

        public DateTime IssueDate { get; set; }

        public override string ToString()
        {
            return $"{base.ToString()}: {FullName} sent a notice on {IssueDate}";
        }
    }
}