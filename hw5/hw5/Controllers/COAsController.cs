﻿using hw5.DAL;
using hw5.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hw5.Controllers
{
    public class COAsController : Controller
    {
        private COAContext db = new COAContext();

        // GET: COAs
        public ActionResult Index()
        {
            return View(db.COAs.ToList());
        }

        /// <summary>
        /// GET: Change of address form
        /// </summary>
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// POST: If the form was filled out correctly it will add it to the database
        /// and redirect the user to the list. If not it will not redirect and leave
        /// the forms filled out.
        /// </summary>
        /// <param name="coa"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form([Bind(Include = "ID,PermitID,FullName,DOB,NewAddress,City,State,ZipCode,County")] COA coa)
        {
            if (ModelState.IsValid)
            {
                coa.IssueDate = DateTime.Today;
                db.COAs.Add(coa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(coa);
        }

        /// <summary>
        /// Clean up method
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}