﻿CREATE TABLE dbo.COAs 
(
	ID			INT				IDENTITY (1,1) NOT NULL,
	PermitID	NVARCHAR(32)	NOT NULL,
	FullName	NVARCHAR(128)	NOT NULL,
	DOB			DATETIME		NOT NULL,
	NewAddress	NVARCHAR(256)	NOT NULL,
	City		NVARCHAR(64)	NOT NULL,
	State		VARCHAR(64)		NOT NULL,
	ZipCode		CHAR(10)		NOT NULL,
	County		VARCHAR(64)		NOT NULL,
	IssueDate	DATETIME		NOT NULL,
	CONSTRAINT	[PK_dbo.COA]	PRIMARY KEY CLUSTERED (ID ASC)
);

INSERT INTO dbo.COAs (PermitID, FullName, DOB, NewAddress, City, State, ZipCode, County, IssueDate) VALUES
	('A012345', 'Reese Ritter',	'08-08-1978', '123 Hankel St', 'Dallas', 'OR', '97338', 'Polk', '09-24-2016'),
	('A045098','Aryan Knox', '12-28-1968', '708 Gwinn St', 'Monmouth', 'OR', '97361', 'Polk', '12-08-2016'),
	('A670934','Abraham Peters', '11-27-1968', '7968 Bald Hill Rd.', 'Crown Point', 'IN', '46307', 'Lake', '04-21-2017'),
	('A000146','Enrique Armstrong', '04-20-1983', '823 Riverview Dr', 'Salem', 'OR', '97304', 'Marion', '04-30-2017'),
	('A037967','Antwan Payne',	'06-10-1989', '423 Bear Creek Rd', 'Bend', 'OR', '97701', 'Deschutes', '08-11-2017');

GO