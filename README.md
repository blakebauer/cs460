# CS460
Contains a portfolio of homework done in my senior course

[Class page](http://www.wou.edu/~morses/classes/cs46x/index.html)
[Portfolio](https://bbauer15.github.io/CS460/)

+ [Homework 1](http://www.wou.edu/~bbauer15/cs460/homework%201/home.html) [Assignment](http://www.wou.edu/~morses/classes/cs46x/assignments/HW1.html)
+ [Homework 2](http://www.wou.edu/~bbauer15/cs460/homework%202/) [Assignment](http://www.wou.edu/~morses/classes/cs46x/assignments/HW2.html)
+ Homework 3 [Assignment](http://www.wou.edu/~morses/classes/cs46x/assignments/HW3.html)
