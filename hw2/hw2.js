var calenderEvents = 1;

$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

function hexToRgb(hex) {
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	} : null;
}

function addNewCalEvent() {
	calenderEvents = calenderEvents + 1;
	var newForm = '<div class="form-group container col-sm-4" id="event_' + calenderEvents + '">' +
		'<div class="col-xs-12">' +
		'<button type="button" class="btn btn_ev_delete col-sm-1" onclick=\'$("#event_' + calenderEvents + '").remove();\'>X</button>' +
		'<div class="row">' +
		'<label for="start" class="col-sm-4">Start Time:</label>' +
		'<input name="start" type="time" class="col-sm-7"></div>' +
		'<div class="row">' +
		'<label for="end" class="col-sm-4">End Time:</label>' +
		'<input name="end" type="time" class="col-sm-7"></div>' +
		'<div class="row">' +
		'<label for="color" class="col-sm-4">Color:</label>' +
		'<input name="color" type="color" class="col-sm-3" value="#ffffff"></div>' +
		'<div class="row">' +
		'<div class="checkbox col-md-4"><label for=""><input type="checkbox" value="" name="M">Monday</label></div>' +
		'<div class="checkbox col-md-4"><label for=""><input type="checkbox" value="" name="T">Tuesday</label></div>' +
		'<div class="checkbox col-md-4"><label for=""><input type="checkbox" value="" name="W">Wednesday</label></div>' +
		'<div class="checkbox col-md-4"><label for=""><input type="checkbox" value="" name="R">Thursday</label></div>' +
		'<div class="checkbox col-md-4"><label for=""><input type="checkbox" value="" name="F">Friday</label></div>' +
		'<div class="checkbox col-md-4"><label for=""><input type="checkbox" value="" name="S">Saturday</label></div>' +
		'<div class="checkbox col-md-4"><label for=""><input type="checkbox" value="" name="U">Sunday</label></div></div>' +
		'<div class="row">' +
		'<textarea name="description" type="text" class="col-sm-12" style="resize: none;" placeholder="Enter a description of the event"></textarea>' +
		'</div></div>';
	$("#addbtnform").before(newForm);
}

/*
 * Creates a time object with a few methods to prevent mishaps of 25 hours or 63 minutes
 */
function Time(hours, minutes) {
  var t = {
	hours: 0,
	minutes: 0,
	toMinutes: function () {
	  return this.minutes + this.hours * 60;
	},
    addTime: function (h, m) {
	  this.hours = (this.hours + h + Math.floor((this.minutes + m) / 60)) % 24;
	  this.minutes = (m + this.minutes) % 60;
	},
    toString: function () {
	  var h = this.hours;
	  var s = h >= 12 ? ' PM' : ' AM';
	  h = h >= 12 ? h - 12 : h;
	  h = h == 0 ? 12 : h;

	  return h + ":" + (this.minutes > 9 ? this.minutes : "0" + this.minutes) + s;
    }
  };
  t.addTime(hours, minutes);

  return t;
}

/*
 * Parses a string to make a time object
 */
function TimeS(s) {
  var m = s.match(/(\d\d):(\d\d)/);
  return Time(parseInt(m[1]), parseInt(m[2]));
}

/* 
 * Subtracts two time objects and returns the minutes
 * Was easier to leave it out of Time object
 */
function subTime(a, b) {
  return (((a.toMinutes() - b.toMinutes()) % 1440) + 1440) % 1440;
}

/*
 * Detemines if x is in between s and e
 */
function between(x, s, e) {
  var diff = subTime(e, s) - 1;
  var xdiff = subTime(x, s);
  return xdiff <= diff;
}

function generateSchedule() {
	//Get all inputs
	var title = $('input[name="title"]')[0].value;
	var textcolor = $('input[name="txt_color"]')[0].value;
	var bgcolor = $('input[name="bgcolor"]')[0].value;
	var bordercolor = $('input[name="border_color"]')[0].value;
	var cbcolor = $('input[name="cb_color"]')[0].value;
	var beginning = TimeS($('input[name="start_time"]')[0].value);
	var end = TimeS($('input[name="end_time"]')[0].value);
	var interval = TimeS($('input[name="int_time"]')[0].value);

	var eventElements = $("[id^=event_]").toArray(); //Gets all the event forms
	var events = [];
	var e;
	
	//Parse all events
	for (e in eventElements) {
		//Check to see if this event has a start and end time
		if (eventElements[e].querySelector('[name="start"]').value == "" || eventElements[e].querySelector('[name="end"]').value == "") {
			alert("Error: Required event field is not filled out!");
			$("#calender>*").remove();
			return;
		}

		//Gets all the day checkboxes and stores them in a list
		var days = [];
		var checkboxes = eventElements[e].querySelectorAll('input[type="checkbox"]');
		var cb;
		for (cb in checkboxes) {
			days.push(checkboxes[cb].checked);
		}

		//Grabs the color and determines to use white text or black for the description
		var temp1 = eventElements[e].querySelector('[name="color"]').value;
		var temp3 = hexToRgb(temp1);

		//Awesome brightness formula for swapping between white and black text http://alienryderflex.com/hsp.html
		var temp2 = (Math.sqrt(0.299 * temp3.r * temp3.r + 0.587 * temp3.g * temp3.g + 0.114 * temp3.b * temp3.b) < 128) ? "#ffffff" : "#000000";
		//Stores the event in an object for easy use
		var event = {
			start: TimeS(eventElements[e].querySelector('[name="start"]').value),
			end: TimeS(eventElements[e].querySelector('[name="end"]').value),
			description: eventElements[e].querySelector('[name="description"]').value,
			color: temp1,
			txtc: temp2,
			days: days
		};
		events.push(event);
	}

	//Add schedule body
	$("#calender>*").remove();
	var schedule = '<center><h3>' + title + '</h3></center><table  style="color:' +
		textcolor + ';background-color:' + bgcolor + ';border-color:' + bordercolor +
		';"><thead><tr><th>Time</th><th>Monday</th><th>Tuesday</th><th>Wednesday</th>' +
		'<th>Thursday</th><th>Friday</th><th>Saturday</th><th>Sunday</th></tr></thead><tbody></tbody></table>';
	$("#calender").append(schedule);

	//Calculate number of rows and construct a time to be incremented
	var diff = (((end.toMinutes() - beginning.toMinutes()) % 1440) + 1440) % 1440;
	var steps = Math.round(diff / interval.toMinutes());
	var timeStep = Time(beginning.hours, beginning.minutes);

	//Add time column
	for (var s = 0; s < steps; s++) {
		$("#calender tbody").append('<tr><td style="border: solid ' + bordercolor + ' 2px">' + timeStep.toString() + "</td></tr>");
		timeStep.addTime(interval.hours, interval.minutes);
	}

	timeStep = Time(beginning.hours, beginning.minutes); //Incremented to track time
	var rows = $("#calender tbody tr"); //Select table rows

	//For each day and each time check to see if theres an event if so place one if not place an empty cell
	for (var d = 0; d < 7; d++) {
		var added = false;
		for (var s = 0; s < steps; s++) {
			for (var i = 0; i < events.length; i++) {
				if (events[i].days[d] && between(timeStep, events[i].start, events[i].end)) {
					var span = Math.ceil(subTime(events[i].end, timeStep) / interval.toMinutes()) - 1; //Row span of the event

					if (span >= steps - s) //Don't span past the end of the table
					{
						span = steps - s - 1;
					}

					if (span < 0) //Error check shouldn't happen anymore but doesn't hurt
					{
						alert("Error: An event is wrapping from one day to the next!");
						$("#calender>*").remove();
						return;
					}

					//Add cell, increment s, and increment time
					$(rows[s]).append('<td rowspan=' + (span + 1) + ' style="color:' + events[i].txtc + ';background-color:' + events[i].color + ';">' + events[i].description + '</td>');
					
					s = s + span;
					timeStep.addTime(interval.hours * span, interval.minutes * span);
					added = true;
					break;
				}
			}

			if (!added) { //Empty cell
				$(rows[s]).append('<td></td>');
			} else {
				added = false;
			}
			timeStep.addTime(interval.hours, interval.minutes);
		}
		timeStep = Time(beginning.hours, beginning.minutes);
	}

	//Styles and scroll down
	$("#calender").append('<style>td{border: solid ' + cbcolor + ' 1px;} th{border-color: ' + bordercolor + ';}</style>');
	document.getElementById("calender").scrollIntoView({
		behavior: "smooth"
	});
}
