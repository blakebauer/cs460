﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hw8.Models
{
    public class ClassificationVM
    {
        public string ArtWorkTitle { get; set; }
        public string GenreName { get; set; }
    }
}