﻿
function loadGenre(id) {
    // Send ajax request
    var response = $.ajax({
        type: "GET",
        dataType: "json",
        url: "Home/GetArtWork",
        data: {
            genreID: id
        }
    }).done(function () {
        // Get table
        var works = $("#works");
        if (works.length == 0)
        {
            // If the table has not been initialized the create it
            works = $("body>div.container").append(`
                <table class="table" id=\"works\">
                  <thead>
                    <tr>
                      <th scope="col">Art Work</ th>
                      <th scope="col">Artist</ th>
                    </ tr>
                  </ thead>
                  <tbody> </ tbody>
                </ table>
            `);
        } else
        {
            // If the table does exist grab it
            works = works[0];
        }
        // Get the body of the table
        var tbody = $("#works tbody");

        // Empty the body
        tbody.empty();

        // Fill the table up with records
        response.responseJSON.forEach(function (m, index) {
            tbody.append(`
        <tr>
          <td>` + m.ArtWork + `</td>
          <td>` + m.Artist + `</td>
        </ tr>
      `);
        });
    });
}