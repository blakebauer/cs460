﻿using hw8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hw8.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            using (ArtContext db = new ArtContext())
            {
                return View(db.Genres.ToList());
            }
        }

        /// <summary>
        /// Returns a list of Artwork title and artist name that fit into the genre.
        /// </summary>
        /// <param name="genreID"></param>
        /// <returns></returns>
        public JsonResult GetArtWork(int genreID)
        {
            using (ArtContext db = new ArtContext())
            {
                List<object> stuff = new List<object>();
                var artworks = db.Genres.Where(g => g.GenreID == genreID).FirstOrDefault().ArtWorks.OrderBy(a => a.Title);


                foreach (var artwork in artworks)
                {
                    stuff.Add(new {
                        ArtWork = artwork.Title,
                        Artist = artwork.Artist.Name
                    });
                }

                return Json(stuff, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ArtWorks()
        {
            using (ArtContext db = new ArtContext())
            {
                return View(db.ArtWorks.Include("Artist").ToList());
            }
        }

        public ActionResult Classifications()
        {
            using (ArtContext db = new ArtContext())
            {
                List<ClassificationVM> classes = new List<ClassificationVM>();

                var artworks = db.ArtWorks.ToList();
                foreach(var artwork in artworks)
                {
                    var genres = artwork.Genres.ToList();
                    foreach(var genre in genres)
                    {
                        classes.Add(new ClassificationVM {
                            GenreName = genre.Name,
                            ArtWorkTitle = artwork.Title
                        });
                    }
                }

                return View(classes);
            }
        }
    }
}