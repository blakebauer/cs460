﻿CREATE TABLE dbo.Artists (
	ArtistID	INT					IDENTITY (1,1) NOT NULL,
	Name		NVARCHAR(128)		NOT NULL,
	BirthDate	Date				NOT NULL,
	BirthCity	NVARCHAR(128)		NOT NULL,
	CONSTRAINT	[PK_Artist]			PRIMARY KEY CLUSTERED (ArtistID ASC)
);

CREATE TABLE dbo.ArtWorks (
	ArtWorkID	INT					IDENTITY (1,1) NOT NULL,
	Title		NVARCHAR(128)		NOT NULL,
	ArtistID	INT					NOT NULL,
	CONSTRAINT	[PK_ArtWork]		PRIMARY KEY CLUSTERED (ArtWorkID ASC),
	CONSTRAINT	[FK_ArtistArtWork]	FOREIGN KEY (ArtistID) REFERENCES Artists(ArtistID)
);

CREATE TABLE dbo.Genres (
	GenreID		INT					IDENTITY (1,1) NOT NULL,
	Name		NVARCHAR(128)		NOT NULL,
	CONSTRAINT	[PK_Genre]			PRIMARY KEY CLUSTERED (GenreID ASC)
);

CREATE TABLE dbo.Classifications (
	ArtWorkID	INT							NOT NULL,
	GenreID		INT							NOT NULL,
	CONSTRAINT	PK_Classification			PRIMARY KEY (ArtWorkID, GenreID),
	CONSTRAINT	[FK_ArtWorkClassification]	FOREIGN KEY (ArtWorkID) REFERENCES ArtWorks(ArtWorkID),
	CONSTRAINT	[FK_GenreClassification]	FOREIGN KEY (GenreID)	REFERENCES Genres(GenreID)
);

INSERT INTO dbo.Artists (Name, BirthDate, BirthCity) VALUES 
	('M. C. Escher',		'06-17-1898', 'Leeuwarden, Netherlands'),
	('Leonardo Da Vinci',	'04-02-1519', 'Vinci, Italy'),
	('Hatip Mehmed Efendi', '11-18-1680', 'Unknown'),
	('Salvador Dali',		'04-11-1904', 'Figueres, Spain');

INSERT INTO dbo.ArtWorks (Title, ArtistID) VALUES
	('Circle Limit III', 1),
	('Twon Tree', 1),
	('Mona Lisa', 2),
	('The Vitruvian Man', 2),
	('Ebru', 3),
	('Honey Is Sweeter Than Blood', 4);

INSERT INTO dbo.Genres (Name) VALUES
	('Tesselation'),
	('Surrealism'),
	('Portrait'),
	('Renaissance');

INSERT INTO dbo.Classifications (ArtWorkID, GenreID) VALUES
	(1, 1), (2, 1), (2, 2), (3, 3), (3, 4), (4, 4), (5, 1), (6, 2);